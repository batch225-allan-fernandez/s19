// JavaScript Conditional Statements - allows us to control the flow of our program


// [SECTION] if, elfe, else statement

/*
Syntax:
	if(condition){
	stamement
	}
*/

// =============================================
// if Statement
// ex.1
let numA = 5;

if(numA > 3) {
	console.log("Hello");
}

// ex.2
let city = "New York"

if (city === "New York") {
	console.log("Welcome to New York City!")
}

// else is Clause
/*
- Executes a statement if previous condition is false and if the specified condition is true
- The "else if" clause is optional and can be aded to capture additional conditions to change the flow of a program

- "else if" won't run if first condition is met (TRUE)
*/

let numB = 1;
numA = 5

// ex. 1
if(numA > 3){
	console.log("Hello") 
} else if (numB > 0) {
	console.log("World")
}

// ex. 2
if(numA < 3){
	console.log("Hello")
} else if (numB > 0) {
	console.log("World")
}

// ex. 3
city = "Tokyo"

if(city === "New York"){
	console.log("Welcome to New York City!")
} else if(city === "Tokyo"){
	console.log("Welcome to Tokyo, Japan!")
}

// ============================================
// else Statement
/*
	-Executes a statement if all other conditions are false
	-The "else" statement is optional and can be added to capture any other result to change the flow of a program.
*/

let numC = -5;
let numD = 7;

if(numC > 0) {
	console.log('Hello');
} else if (numD === 0) {
	console.log('World');
} else {
	console.log('Again');
}

// if, else if and else statement with function

function determineTyphoonIntensity(windSpeed){
	if(windSpeed < 30) {
		return "Not a typhoon yet"
	}
	else if(windSpeed <= 61) {
		return "Tropical deperession detected"
	}
	else if(windSpeed >= 62 && windSpeed <= 88){
		return "Tropical storm detected"
	}
	else if(windSpeed >= 89 &&  windSpeed <= 117){
		return "Severe tropical strom detected"
	}
	else {
		return "Typhoon detected"
	}
}

let message = determineTyphoonIntensity(87)
console.log(message)

if (message === "Tropical storm detected"){
	console.warn(message)
}

// Conditional (Ternary Operator)
/*
	- the Conditional (Ternary) Operator takes in three operands:
	1. condition
	2. expression to execute of the condition id truthy
	3. expression to execute if the confition is falsy

	- Ternary Operator is for shorthand code - commonly used for single statement execution where the result consists of only one line of code

	- can be used as an alternative to an "if else" statement
	- Syntax
		(expression) ? ifTrue : ifFalse
*/

// Single statement execution

let t = "yes"
let f = "no"

let ternaryResult = (1 < 18) ? t : f
console.log("Result of Ternary Operator: " + ternaryResult)

let ternaryResult2 = (36 < 18) ? true : false
console.log("Result of Ternary Operator: " + ternaryResult2)

// =======================================

// Multiple Statement execution

let name;

function isOfLegalAge() {
    name = 'John';
    return 'You are of the legal age limit';
}

function isUnderAge() {
    name = 'Jane';
    return 'You are under the age limit';
}

// The "parseInt" function converts the input receive into a number data type.

let age = parseInt(prompt("What is your age?"));
console.log(age);
let legalAge = (age >= 18) ? isOfLegalAge() : isUnderAge();
console.log("Result of Ternary Operator in functions: " + legalAge + ', ' + name);

// =======================================================
//  The SWITCH STATEMENT
/*
	- the switch statements evaluates an expression and matches the expression's value to a case clause. The switch will then execute the statements associated with that case, as well as statements in cases that follow the matching case.

	- can be used as an alternativ to an "if", "else if" and "else" statements where the date to be used in the condition is an expected output
*/
/*
	switch (expression) {
		case value:
			statement;
			break;
		default: statement;
	}
*/

let day = prompt("What day of the week is it today?").toLowerCase();
	console.log(day);

switch (day) {
    case 'monday': 
        console.log("The color of the day is red");
        break;
    case 'tuesday':
        console.log("The color of the day is orange");
        break;
    case 'wednesday':
        console.log("The color of the day is yellow");
        break;
    case 'thursday':
        console.log("The color of the day is green");
        break;
    case 'friday':
        console.log("The color of the day is blue");
        break;
    case 'saturday':
        console.log("The color of the day is indigo");
        break;
    case 'sunday':
        console.log("The color of the day is violet");
        break;
    default:
        console.log("Please input a valid day");
        break;
}

